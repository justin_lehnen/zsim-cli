pyinstaller -n zsim-cli -i bin/logo.ico --distpath bin --workpath temp --onefile --clean src/entry.py
rm -rf temp

py -m build
py -m twine upload dist/*
