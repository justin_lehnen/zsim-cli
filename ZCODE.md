<div id="top"></div>

# Z-Code syntax

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#token-definitions">Token definitions</a></li>
    <li><a href="#syntax-diagram">Syntax diagram</a></li>
    <li>
      <a href="#full-list-of-commands">Full list of commands</a>
      <ul>
        <li><a href="#z">Z</a></li>
        <li><a href="#zfp">ZFP</a></li>
        <li><a href="#zds">ZDS</a></li>
      </ul>
    </li>
    <li>
      <a href="#writing-z-code">Writing Z-Code</a>
      <ul>
        <li><a href="#basics">Basics</a></li>
      </ul>
    </li>
  </ol>
</details>

## Token definitions

The tokens for the tokenization of the Z-Code are defined with the following regular expressions:
* `newline` = `\r?\n`
* `number` = `0 | -?[1-9][0-9]*`
* `identifier` = `[a-zA-Z][a-zA-Z0-9_]*`
* `command_0` = `POP|ADD|SUB|MULT|DIV|NOP|HALT|RET|EQ|NE|LT|GT|LE|GE|LODI|STOI|PRT|PRN|PRC`
* `command_1` = `LIT|LOD(LOCAL)?|STO(LOCAL)?|AC`
* `command_label` = `JMP|JMC`
* `command_call` = `CALL`

Or in EBNF:
```ebnf
letter = 'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G'
       | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' | 'N'
       | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U'
       | 'V' | 'W' | 'X' | 'Y' | 'Z' | 'a' | 'b'
       | 'c' | 'd' | 'e' | 'f' | 'g' | 'h' | 'i'
       | 'j' | 'k' | 'l' | 'm' | 'n' | 'o' | 'p'
       | 'q' | 'r' | 's' | 't' | 'u' | 'v' | 'w'
       | 'x' | 'y' | 'z' ; 
digit = '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' ;
positive_digit = '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' ;

newline = [ '\r' ] , '\n' ;
number = '0' | ( [ '-' ] , positive_digit , { digit } ) ;
identifier = letter , { letter | digit | '_' } ;
command_0 = 'POP' | 'ADD' | 'SUB' | 'MULT' | 'DIV' | 'NOP'
        | 'HALT' | 'RET' | 'EQ' | 'NE' | 'LT' | 'GT' | 'LE' | 'GE'
        | 'LODI' | 'STOI' | 'PRT' | 'PRN' | 'PRC' ;
command_1 = 'LIT' | 'LODLOCAL' | 'LOD' | 'STOLOCAL' | 'STO' | 'AC' ;
command_label = 'JMP' | 'JMC' ;
command_call = 'CALL' ;
```

<div align="right">(<a href="#top">back to top</a>)</div>

## Syntax diagram

### EBNF
```ebnf
program = [ { newline } ] [ memory_declaration ] [ { newline } ] { statement } ;
memory_declaration = '[' , [ memory_assignment | { memory_assignment , ',' } ] , ']' , newline ;
memory_assignment = number , '/' , number ;
statement = [ { label } ] , command , ';' , [ { label } ] ;
label = identifier , ':' , [ { newline } ] ;
command = command_0
        | ( command_1 , number )
        | ( command_label , ( number | identifier ) )
        | ( command_call , '(' , call_parameters , ')' ) ;
call_parameters = ( number | identifier ) , ',' , number , ',' , [ { number } ] ;
```

### Program
![Program][program-diagram-url]

### MemoryDeclaration
![MemoryDeclaration][memory-diagram-url]

### MemoryAssignment
![MemoryAssignment][assignment-diagram-url]

### Statement
![Statement][statement-diagram-url]

### Label
![Label][label-diagram-url]

### Command
![Command][command-diagram-url]

### CallParameters
![CallParameters][callparams-diagram-url]

<div align="right">(<a href="#top">back to top</a>)</div>

## Full list of commands

### Z

#### LIT
```
(LIT z)(m, d, b, h, o) = (m+1, z:d, b, h, o)
```
Pushes a single integer onto the stack.

<div align="right">(<a href="#top">back to top</a>)</div>

#### POP
```
(POP)(m, z:d, b, h, o) = (m+1, d, b, h, o)
```
Removes the first integer from the stack.

<div align="right">(<a href="#top">back to top</a>)</div>

#### NOP
```
(NOP)(m, d, b, h, o) = (m+1, d, b, h, o)
```
No operation. Only increases the instruction counter.

<div align="right">(<a href="#top">back to top</a>)</div>

#### LOD
```
(LOD n)(m, d, b, h, o) = (m+1, h(n):d, b, h, o)
```
Loads the address `n` from the heap memory and push it onto the stack.

<div align="right">(<a href="#top">back to top</a>)</div>

#### STO
```
(STO n)(m, z:d, b, h, o) = (m+1, d, b, h[n/z], o)
```
Stores the first integer on the stack in the heap at address `n`.

<div align="right">(<a href="#top">back to top</a>)</div>

#### PRN
```
(PRN)(m, z:d, b, h, o) = (m+1, d, b, h, oz\n)
```
Prints the first integer on the stack ending with a line break. See [PRT](#prt) for more information.

<div align="right">(<a href="#top">back to top</a>)</div>

#### PRT
```
(PRT)(m, z:d, b, h, o) = (m+1, d, b, h, oz)
```
Prints the first integer on the stack. The result is stored in the `o` component of the machine.

<div align="right">(<a href="#top">back to top</a>)</div>

#### PRC
```
(PRT)(m, z:d, b, h, o) = (m+1, d, b, h, ochar(z))
```
Prints the first integer converted to a character to the output. PRC with 65 on the stack would print `A` for example.

<div align="right">(<a href="#top">back to top</a>)</div>

#### ADD
```
(ADD)(m, z1 z2:d, b, h, o) = (m+1, (z1+z2):d, b, h, o)
```
Adds the first two integer on the stack together and push it back onto the stack.

<div align="right">(<a href="#top">back to top</a>)</div>

#### SUB
```
(SUB)(m, z1 z2:d, b, h, o) = (m+1, (z2-z1):d, b, h, o)
```
Subtracts the first two integer on the stack together and push it back onto the stack.

<div align="right">(<a href="#top">back to top</a>)</div>

#### MULT
```
(MULT)(m, z1 z2:d, b, h, o) = (m+1, (z1*z2):d, b, h, o)
```
Multiplies the first two integer on the stack together and push it back onto the stack.

<div align="right">(<a href="#top">back to top</a>)</div>

#### DIV
```
(DIV)(m, z1 z2:d, b, h, o) = (m+1, (z2/z1):d, b, h, o)
```
Divides the first two integer on the stack together and push it back onto the stack.

<div align="right">(<a href="#top">back to top</a>)</div>

#### JMP
```
(JMP n)(m, d, b, h, o) =
        if n > 0 then (n, d, b, h, o)
        if n < 0 then (stopaddr + n, d, b, h, o)
        if n = 0 then (stopaddr, d, b, h, o)
```
Manipulates the instruction counter. Execution will continue at instruction `n`.<br />
If `n` is zero the command behaves exactly like [HALT](#halt).<br />
If `n` is less than zero the command will set the instruction counter to `n` instructions before the end.

<div align="right">(<a href="#top">back to top</a>)</div>

#### JMC
```
(JMC n)(m, b:d, b, h, o) = 
        if b = 0 then
          if n > 0 then (n, d, b, h, o)
          if n < 0 then (stopaddr + n, d, b, h, o)
          if n = 0 then (stopaddr, d, b, h, o)
        if b = 1 then (m+1, d, b, h, o)
```
Applies a conditional [JMP](#jmp). This command behaves like [JMP](#jmp) if the first element on the stack is equal to **zero**, otherwise like [NOP](#nop).<br />
The check against **zero** is chosen to support a programming style similar to an "if"-statement.

<div align="right">(<a href="#top">back to top</a>)</div>

#### EQ
```
(EQ)(m, z1 z2:d, b, h, o) =
        if z2 EQ z1 = true then (m+1, 1:d, b, h, o)
        if z2 EQ z1 = false then (m+1, 0:d, b, h, o)
```
Pushes `1` onto the stack if the second integer is equal to the first integer on the stack and `0` otherwise.

<div align="right">(<a href="#top">back to top</a>)</div>

#### NE
```
(NE)(m, z1 z2:d, b, h, o) =
        if z2 NE z1 = true then (m+1, 1:d, b, h, o)
        if z2 NE z1 = false then (m+1, 0:d, b, h, o)
```
Pushes `1` onto the stack if the second integer is not equal to the first integer on the stack and `0` otherwise.

<div align="right">(<a href="#top">back to top</a>)</div>

#### LT
```
(LT)(m, z1 z2:d, b, h, o) =
        if z2 LT z1 = true then (m+1, 1:d, b, h, o)
        if z2 LT z1 = false then (m+1, 0:d, b, h, o)
```
Pushes `1` onto the stack if the second integer is less than the first integer on the stack and `0` otherwise.

<div align="right">(<a href="#top">back to top</a>)</div>

#### GT
```
(GT)(m, z1 z2:d, b, h, o) =
        if z2 GT z1 = true then (m+1, 1:d, b, h, o)
        if z2 GT z1 = false then (m+1, 0:d, b, h, o)
```
Pushes `1` onto the stack if the second integer is greater than the first integer on the stack and `0` otherwise.

<div align="right">(<a href="#top">back to top</a>)</div>

#### LE
```
(LE)(m, z1 z2:d, b, h, o) =
        if z2 LE z1 = true then (m+1, 1:d, b, h, o)
        if z2 LE z1 = false then (m+1, 0:d, b, h, o)
```
Pushes `1` onto the stack if the second integer is less than or equal to the first integer on the stack and `0` otherwise.

<div align="right">(<a href="#top">back to top</a>)</div>

#### GE
```
(GE)(m, z1 z2:d, b, h, o) =
        if z2 GE z1 = true then (m+1, 1:d, b, h, o)
        if z2 GE z1 = false then (m+1, 0:d, b, h, o)
```
Pushes `1` onto the stack if the second integer is greater than or equal to the first integer on the stack and `0` otherwise.

<div align="right">(<a href="#top">back to top</a>)</div>


### ZFP

#### LODLOCAL
```
(LODLOCAL i)(m, d, b, h, o) = (m+1, b(b(1)-i+1):d, b, h, o)
```
Load the `i`-th variable in the procedure stack's current call frame and pushes it onto the stack.

<div align="right">(<a href="#top">back to top</a>)</div>

#### STOLOCAL
```
(STOLOCAL i)(m, z:d, b, h, o) = (m+1, d, b[(b(1)-i+1)/z], h, o)
```
Stores the integer on the stack in the `i`-th variable in the procedure stack's current call frame.

<div align="right">(<a href="#top">back to top</a>)</div>

#### CALL
```
(CALL(ca, npar, lv1 ... lvn))(m, pnpar ... p1:d, b, h, o) =
        (ca, d, (npar+n+2) (m+1) lvn ... lv1 pnpar ... p1:b, h, o)
```
Calls a procedure/function.<br />
Execution continues at the address `ca`.<br />
`npar` integers are taken from the stack and pushed onto the procedure stack in the **same order**.<br />
Local parameters defined in `lv1 ... lvn` and pushed onto the procedure stack in **reverse order**.<br />
Finally the return address and the size of the call frame are pushed onto the procedure stack.

<div align="right">(<a href="#top">back to top</a>)</div>

#### RET
```
(RET)(m, d, (npar+n+2) ra lvn ... lv1 pnpar ... p1:b, h, o) =
        (ra, d, b, h, o)
```
Equivalent to a return command.<br />
Takes the call frame off the procedure stack.<br />
Ends execution of a function and continues the execution where the function was called.<br />

<div align="right">(<a href="#top">back to top</a>)</div>

#### HALT
```
(HALT)(m, d, b, h, o) = (stopaddr, d, b, h, o)
```
Ends execution of the current code by jumping past the last instruction.<br />
Equivalent to [JMP 0](#jmp).


<div align="right">(<a href="#top">back to top</a>)</div>


### ZDS

#### LODI
```
(LODI)(m, a:d, b, h, o) = (m+1, h(a):d, b, h, o)
```
[LOD](#lod) indirect. Loads by using the integer on the stack as an address.<br />
`LIT n; LODI;` is equivalent to `LOD n;`.

<div align="right">(<a href="#top">back to top</a>)</div>

#### STOI
```
(STOI)(m, z a:d, b, h, o) = (m+1, d, b, h[a/z], o)
```
[STO](#sto) indirect. Stores an integer `z` on the stack at address `a` which is also taken from the stack.<br />
`LIT n; LIT z; STOI;` is equivalent to `LIT z; STO n;`.

<div align="right">(<a href="#top">back to top</a>)</div>

#### AC
```
(AC n)(m, i:d, b, h, o) =
      if 0 <= i < n then (m+1, i:d, b, h, o)
      else throw Exception
```
Performs an index check.<br />
Execution is continued if the integer `i` on the stack is greater or equal to 0 and less than the parameter `n`.

<div align="right">(<a href="#top">back to top</a>)</div>


## Writing Z-Code

### Basics
1. There is only one data type in Z-Code, which is _integer_.<br />
The only exception to this is the output which is a string, but _read-only_.

1. Z-Code uses its _stack_ for most operations. If you are not familiar with stacks, just think of a stack of cards - [last in, first out](https://en.wikipedia.org/wiki/Stack_(abstract_data_type)).

1. Operations are typically done using postfix notation.<br />
For example: `6 5 +` results in `11`.

1. Comments are initiated with a `#`.

<div align="right">(<a href="#top">back to top</a>)</div>

### Examples

#### Variable declaration
```
# int var1 = 5;
LIT 5;
STO 1;
```
<div align="right">(<a href="#top">back to top</a>)</div>

#### Arithmetic
```
# 100 / ((var1 + var2) * var2)
[1/1, 2/-5]
LIT 100;
# (var1 + var2)
LOD 1;
LOD 2;
ADD;
# (var1 + var2) * var2
LOD 2;
MULT;
# 100 / (var1 + var2) * var2
DIV;
STO 3;
```
<div align="right">(<a href="#top">back to top</a>)</div>

#### If-statement
```
# In this example we construct a C-style if-statement:
# if (var1 < var2) print(var2 - var1)
[1/2, 2/5]
LOD 1;
LOD 2;
LT;
JMC else;
# This is inside the if-statement
LOD 2;
LOD 1;
SUB;
PRT;
# Here our if-statement is over
else:
```
<div align="right">(<a href="#top">back to top</a>)</div>

#### For-loop
```
# for (int i = 0; i < 10; i++) print(i);
[1/0, 2/10]
LOD 1;
LOD 2;
CALL(for, 2,);
HALT;

for:
# i < n
LODLOCAL 1;
LODLOCAL 2;
LT;
JMC end;

# print(i)
LODLOCAL 1;
PRT;

# i++
LODLOCAL 1;
LIT 1;
ADD;
STOLOCAL 1;
JMP for;
for_end: RET;
```
<div align="right">(<a href="#top">back to top</a>)</div>

#### Calculating pow(b, e) recursively
```
[1/2, 2/3] # 2^3
LOD 1;
LOD 2;
CALL(pow, 2, );
PRT;
HALT;
pow:

# Check if e equals 0 => 1
LODLOCAL 2;
LIT 0;
EQ;
JMC e_equals_0;
LIT 1;
RET;
e_equals_0:

# Check if b equals 0 => 0
LODLOCAL 1;
LIT 0;
EQ;
JMC b_equals_0;
LIT 0;
RET;
b_equals_0:

LODLOCAL 1;

# Calculate b^(e-1)
LODLOCAL 1;
LODLOCAL 2;
LIT 1;
SUB;
CALL(pow, 2, );

# Return b * b^(e-1)
MULT;
pow_end: RET;
```
<div align="right">(<a href="#top">back to top</a>)</div>

#### Calculating pow(b, e) iteratively
```
[1/2, 2/3] # 2^3
LOD 1;
LOD 2;
CALL(pow, 2, 0);
PRT;
HALT;
pow:
# Save a copy of our base as a reduce variable
LODLOCAL 1;
STOLOCAL 3;

# Check if e equals 0 => 1
LODLOCAL 2;
LIT 0;
EQ;
JMC e_equals_0;
LIT 1;
RET;
e_equals_0:

# Check if b equals 0 => 0
LODLOCAL 1;
LIT 0;
EQ;
JMC b_equals_0;
LIT 0;
RET;
b_equals_0:

# Decrement counter and abort at <= 0
LODLOCAL 2;
LIT 1;
GT;
JMC pow_end;
LODLOCAL 2;
LIT 1;
SUB;
STOLOCAL 2;

# Multiply
LODLOCAL 1;
LODLOCAL 3;
MULT;
STOLOCAL 3;
JMP b_equals_0;

pow_end:
LODLOCAL 3;
RET;
```

<div align="right">(<a href="#top">back to top</a>)</div>

[program-diagram-url]: images/diagram_program.png
[memory-diagram-url]: images/diagram_memory_decl.png
[assignment-diagram-url]: images/diagram_memory_assign.png
[statement-diagram-url]: images/diagram_statement.png
[label-diagram-url]: images/diagram_label.png
[command-diagram-url]: images/diagram_command.png
[callparams-diagram-url]: images/diagram_callparams.png
