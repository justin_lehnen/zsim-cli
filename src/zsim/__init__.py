from .cli import entry_point
from .machine import Machine
from .parser import Tokenizer
from .parser import RecursiveDescent
from .zcode_parser import ZCodeParser
from .zcode_statement_parser import ZCodeStatementParser
from .zcode_tokenizer import tokenizer


if __name__ == '__main__':
    entry_point()
