from typing import NamedTuple

class Token(NamedTuple):
    type: str
    value: str
