from .instruction import Instruction
from .jump_instruction import JumpInstruction
from .call_instruction import CallInstruction
