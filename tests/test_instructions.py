from zsim.instructions import Instruction, JumpInstruction, CallInstruction


def test_Instruction_mnemonic():
    instruction = Instruction('MNEMONIC', None)
    assert instruction.mnemonic == 'MNEMONIC'

def test_Instruction_parameters_length():
    instruction = Instruction('MNEMONIC', None, 1, 2, 3)
    assert len(instruction.parameters) == 3

def test_Instruction_parameters():
    instruction = Instruction('MNEMONIC', None, 1, 2, 3)
    assert instruction.parameters == (1, 2, 3)


def test_JumpInstruction_mnemonic():
    instruction = JumpInstruction('MNEMONIC', None)
    assert instruction.mnemonic == 'MNEMONIC'

def test_JumpInstruction_parameters_length():
    instruction = JumpInstruction('MNEMONIC', None, 1, 2, 3)
    assert len(instruction.parameters) == 3

def test_JumpInstruction_parameters():
    instruction = JumpInstruction('MNEMONIC', None, 1, 2, 3)
    assert instruction.parameters == (1, 2, 3)


def test_CallInstruction_mnemonic():
    instruction = CallInstruction('MNEMONIC', None)
    assert instruction.mnemonic == 'MNEMONIC'

def test_CallInstruction_parameters_length():
    instruction = CallInstruction('MNEMONIC', None, 1, 2, 3)
    assert len(instruction.parameters) == 3

def test_CallInstruction_parameters():
    instruction = CallInstruction('MNEMONIC', None, 1, 2, 3)
    assert instruction.parameters == (1, 2, 3)

