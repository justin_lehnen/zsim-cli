from zsim.parser import TokenType

def test_token_type():
    token = TokenType(type='NUMERAL', regex=r'0|-?[1-9][0-9]*', skip=False)
    assert token.type == 'NUMERAL'

def test_token_type_type():
    token = TokenType(type='NUMERAL', regex=r'0|-?[1-9][0-9]*', skip=False)
    assert type(token.type) == str

def test_token_regex():
    token = TokenType(type='NUMERAL', regex=r'0|-?[1-9][0-9]*', skip=False)
    assert token.regex == r'0|-?[1-9][0-9]*'

def test_token_regex_type():
    token = TokenType(type='NUMERAL', regex=r'0|-?[1-9][0-9]*', skip=False)
    assert type(token.regex) == str

def test_token_skip():
    token = TokenType(type='NUMERAL', regex=r'0|-?[1-9][0-9]*', skip=False)
    assert not token.skip

def test_token_skip_type():
    token = TokenType(type='NUMERAL', regex=r'0|-?[1-9][0-9]*', skip=False)
    assert type(token.skip) == bool
