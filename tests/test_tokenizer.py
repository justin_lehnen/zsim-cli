from zsim.parser import Tokenizer

def test_creation_tokenizer():
    tokenizer = Tokenizer()
    assert tokenizer.default == 'unknown'
    assert tokenizer.eof == 'eof'

def test_creation_tokenizer_2():
    tokenizer = Tokenizer(default='default', eof=None, skip='__skip__')
    assert tokenizer.default == 'default'
    assert tokenizer.default_skip == '__skip__'
    assert tokenizer.eof == None

def test_tokenizer_token():
    tokenizer = Tokenizer()
    tokenizer.token('number', r'0', r'-?[1-9][0-9]*')
    assert tokenizer.get_types()[0].type == 'number'
    assert tokenizer.get_types()[0].regex == r'0'
    assert not tokenizer.get_types()[0].skip
    assert tokenizer.get_types()[1].type == 'number'
    assert tokenizer.get_types()[1].regex == r'-?[1-9][0-9]*'
    assert not tokenizer.get_types()[1].skip

def test_tokenizer_skip():
    tokenizer = Tokenizer()
    tokenizer.skip(r'\s+')
    assert tokenizer.get_types()[0].type == 'skip'
    assert tokenizer.get_types()[0].regex == r'\s+'
    assert tokenizer.get_types()[0].skip

def test_tokenizer_tokenize():
    tokenizer = (Tokenizer(eof=None)
        .token('number', r'0|-?[1-9][0-9]*')
        .token('operator', r'\+|-|\*|/')
        .skip(r'\s+'))

    tokens = tokenizer.tokenize('10 + -5 * 10 / 2')
    assert len(tokens) == 7
    assert tokens[0] == ('number', '10')
    assert tokens[1] == ('operator', '+')
    assert tokens[2] == ('number', '-5')
    assert tokens[3] == ('operator', '*')
    assert tokens[4] == ('number', '10')
    assert tokens[5] == ('operator', '/')
    assert tokens[6] == ('number', '2')
