from zsim.instruction_sets import InstructionSet, ZInstructions, ZFPInstructions, ZDSInstructions, ZFPDSInstructions
from zsim.instructions import Instruction
from zsim import Machine


def test_instruction_LODLOCAL():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.b = [10, 20, -3, 2, 5]

    assert machine.m == 1
    assert len(machine.b) == 5
    assert machine.b[0] == 10
    assert machine.b[1] == 20
    assert machine.b[2] == -3
    assert machine.b[3] == 2
    assert machine.b[4] == len(machine.b)
    zfpds('LODLOCAL', machine, 1)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == 10

def test_instruction_STOLOCAL():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [7]
    machine.b = [10, 20, -3, 2, 5]

    assert machine.m == 1
    assert len(machine.d) == 1
    assert machine.d[0] == 7
    assert len(machine.b) == 5
    assert machine.b[0] == 10
    assert machine.b[1] == 20
    assert machine.b[2] == -3
    assert machine.b[3] == 2
    assert machine.b[4] == len(machine.b)
    zfpds('STOLOCAL', machine, 1)
    assert machine.m == 2
    assert len(machine.d) == 0
    assert len(machine.b) == 5
    assert machine.b[0] == 7
    assert machine.b[1] == 20

def test_instruction_CALL():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [10, 20]

    assert machine.m == 1
    assert len(machine.d) == 2
    assert machine.d[0] == 10
    assert machine.d[1] == 20
    assert len(machine.b) == 0
    zfpds('CALL', machine, 99, 2, [-3])
    assert machine.m == 99
    assert len(machine.d) == 0
    assert len(machine.b) == 5
    assert machine.b[0] == 10
    assert machine.b[1] == 20
    assert machine.b[2] == -3
    assert machine.b[3] == 2
    assert machine.b[4] == len(machine.b)

def test_instruction_RET():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.b = [10, 20, -3, 2, 5]

    assert machine.m == 1
    assert len(machine.b) == 5
    assert machine.b[0] == 10
    assert machine.b[1] == 20
    assert machine.b[2] == -3
    assert machine.b[3] == 2
    assert machine.b[4] == len(machine.b)
    zfpds('RET', machine)
    assert machine.m == 2
    assert len(machine.b) == 0

def test_instruction_HALT():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.instructions = [
        Instruction('NOP', zfpds.NOP), # 1 or -3
        Instruction('NOP', zfpds.NOP), # 2 or -2
        Instruction('NOP', zfpds.NOP), # 3 or -1
        # 0
    ]

    assert machine.m == 1
    zfpds('HALT', machine)
    assert machine.m == 4
