from zsim.parser import Token

def test_token_type():
    token = Token(type='NUMERAL', value='1234')
    assert token.type == 'NUMERAL'

def test_token_type_type():
    token = Token(type='NUMERAL', value='1234')
    assert type(token.type) == str

def test_token_value():
    token = Token(type='NUMERAL', value='1234')
    assert token.value == '1234'

def test_token_value_type():
    token = Token(type='NUMERAL', value='1234')
    assert type(token.value) == str
