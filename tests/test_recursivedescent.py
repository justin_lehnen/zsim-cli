from zsim.parser import Token, RecursiveDescent, ParseException

tokens = [
    Token('opening_parenthesis', '('),
    Token('number', '10'),
    Token('operator_add', '+'),
    Token('number', '-5'),
    Token('closing_parenthesis', ')'),
    Token('operator_mult', '*'),
    Token('number', '10'),
    Token('operator_mult', '/'),
    Token('number', '2'),
]

class ExprParser(RecursiveDescent):
    def __init__(self, tokens):
        super(ExprParser, self).__init__(tokens)

    def program(self):
        return self.__expression()

        
    def __expression(self):
        left = self.__product()
        while self.accept('operator_add', advance=False):
            operator = self.token.value
            self.advance()
            right = self.__product()
            left += right + operator + ' '
        return left

    def __product(self):
        left = self.__term()
        while self.accept('operator_mult', advance=False):
            operator = self.token.value
            self.advance()
            right = self.__term()
            left += right + operator + ' '
        return left

    def __term(self):
        if self.accept('number', advance=False):
            number = self.token.value
            self.advance()
            return number + ' '

        self.expect('opening_parenthesis')
        expression = self.__expression()
        self.expect('closing_parenthesis')
        return expression


def test_recursivedescent_token():
    parser = ExprParser(tokens)
    assert parser.token == tokens[0]
    assert not parser.previous
    assert parser.next == tokens[1]

def test_recursivedescent_accept():
    parser = ExprParser(tokens)
    assert parser.accept('number', 'opening_parenthesis')

def test_recursivedescent_expect():
    parser = ExprParser(tokens)
    try:
        parser.expect('number', 'opening_parenthesis')
        assert True
    except:
        assert False

def test_recursivedescent_expect_false():
    parser = ExprParser(tokens)
    try:
        parser.expect('operator')
        assert False
    except:
        assert True

def test_recursivedescent_error():
    parser = ExprParser(tokens)
    try:
        parser.error('number')
        assert False
    except:
        assert True

def test_recursivedescent_valid():
    parser = ExprParser(tokens)
    assert parser.valid()

def test_recursivedescent_postfix():
    parser = ExprParser(tokens)
    assert parser.program().strip() == '10 -5 + 10 * 2 /'.strip()
