from zsim.instruction_sets import InstructionSet, ZInstructions, ZFPInstructions, ZDSInstructions, ZFPDSInstructions
from zsim.instructions import Instruction
from zsim import Machine


def test_instruction_LODI():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [], { 5: -2 })
    machine.d = [5]

    assert machine.m == 1
    assert len(machine.d) == 1
    assert machine.d[0] == 5
    assert len(machine.h) == 1
    assert machine.h[5] == -2
    zfpds('LODI', machine)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == -2

def test_instruction_STOI():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [], { 5: -2 })
    machine.d = [5, 4]

    assert machine.m == 1
    assert len(machine.d) == 2
    assert machine.d[0] == 5
    assert machine.d[1] == 4
    assert len(machine.h) == 1
    assert machine.h[5] == -2
    zfpds('STOI', machine)
    assert machine.m == 2
    assert len(machine.d) == 0
    assert machine.h[5] == 4

def test_instruction_AC():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [5]

    assert machine.m == 1
    assert len(machine.d) == 1
    assert machine.d[0] == 5
    zfpds('AC', machine, 10)
    assert machine.m == 2
    assert len(machine.d) == 1

def test_instruction_AC_false_negative():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [-1]

    assert machine.m == 1
    assert len(machine.d) == 1
    assert machine.d[0] == -1

    try:
        zfpds('AC', machine, 10)
        assert False
    except:
        assert True

    assert machine.m == 1
    assert len(machine.d) == 1

def test_instruction_AC_false_high():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [20]

    assert machine.m == 1
    assert len(machine.d) == 1
    assert machine.d[0] == 20

    try:
        zfpds('AC', machine, 10)
        assert False
    except:
        assert True

    assert machine.m == 1
    assert len(machine.d) == 1
