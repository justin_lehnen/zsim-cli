from zsim import scanner, ZCodeParser, Machine
from zsim.instruction_sets import ZFPDSInstructions


def test_sample_ack11():
    file = open('programs/ack11.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 43
    assert len(machine.d) == 0
    assert len(machine.b) == 0
    assert len(machine.h) == 1
    assert len(machine.o) == 1
    assert machine.h[1] == 2
    assert machine.o == '2'

def test_sample_arithmetic():
    file = open('programs/arithmetic.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 6
    assert len(machine.d) == 1
    assert len(machine.b) == 0
    assert len(machine.h) == 0
    assert len(machine.o) == 0
    assert machine.d[0] == 55

def test_sample_comments():
    file = open('programs/comments.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 9
    assert len(machine.d) == 1
    assert len(machine.b) == 0
    assert len(machine.h) == 1
    assert len(machine.o) == 3
    assert machine.d[0] == -5
    assert machine.h[1] == 10
    assert machine.o == '710'

def test_sample_comments2():
    file = open('programs/comments2.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 3
    assert len(machine.d) == 0
    assert len(machine.b) == 0
    assert len(machine.h) == 1
    assert len(machine.o) == 2
    assert machine.h[1] == 10
    assert machine.o == '10'

def test_sample_datastructures():
    file = open('programs/datastructures.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 41
    assert len(machine.d) == 0
    assert len(machine.b) == 0
    assert len(machine.h) == 9
    assert len(machine.o) == 0
    assert machine.h[0] == 9
    assert machine.h[1] == 3
    assert machine.h[2] == 0
    assert machine.h[3] == 42
    assert machine.h[4] == 84
    assert machine.h[5] == 0
    assert machine.h[6] == 3
    assert machine.h[7] == 8
    assert machine.h[8] == 42

def test_sample_dsfunproc():
    file = open('programs/dsfunproc.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 19
    assert len(machine.d) == 0
    assert len(machine.b) == 0
    assert len(machine.h) == 3
    assert len(machine.o) == 1
    assert machine.h[0] == 11
    assert machine.h[1] == 10
    assert machine.h[10] == 65
    assert machine.o == 'A'

def test_sample_dstest():
    file = open('programs/dstest.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 57
    assert len(machine.d) == 0
    assert len(machine.b) == 0
    assert len(machine.h) == 9
    assert len(machine.o) == 0
    assert machine.h[0] == 9
    assert machine.h[1] == 3
    assert machine.h[2] == 0
    assert machine.h[3] == 42
    assert machine.h[4] == 84
    assert machine.h[5] == 0
    assert machine.h[6] == 3
    assert machine.h[7] == 8
    assert machine.h[8] == 42

def test_sample_empty():
    file = open('programs/empty.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) == 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert not parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) == 0

    machine.run()
    assert machine.m == 1
    assert len(machine.d) == 0
    assert len(machine.b) == 0
    assert len(machine.h) == 0
    assert len(machine.o) == 0

def test_sample_fib2():
    file = open('programs/fib2.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 23
    assert len(machine.d) == 0
    assert len(machine.b) == 0
    assert len(machine.h) == 1
    assert len(machine.o) == 1
    assert machine.h[1] == 2
    assert machine.o == '2'

def test_sample_functest():
    file = open('programs/functest.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 14
    assert len(machine.d) == 0
    assert len(machine.b) == 0
    assert len(machine.h) == 0
    assert len(machine.o) == 5
    assert machine.o == '3\n30\n'

def test_sample_helloworld():
    file = open('programs/helloworld.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 20
    assert len(machine.d) == 0
    assert len(machine.b) == 0
    assert len(machine.h) == 1
    assert len(machine.o) == len('Hello World\n')
    assert machine.h[1] == 0
    assert machine.o == 'Hello World\n'

def test_sample_loops():
    file = open('programs/loops.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 16
    assert len(machine.d) == 0
    assert len(machine.b) == 0
    assert len(machine.h) == 2
    assert len(machine.o) == 0
    assert machine.h[1] == 7
    assert machine.h[2] == 10

def test_sample_looptest():
    file = open('programs/looptest.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 12
    assert len(machine.d) == 0
    assert len(machine.b) == 0
    assert len(machine.h) == 1
    assert len(machine.o) == len('9876543210')
    assert machine.h[1] == 0
    assert machine.o == '9876543210'

def test_sample_memory():
    file = open('programs/memory.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 6
    assert len(machine.d) == 1
    assert len(machine.b) == 0
    assert len(machine.h) == 1
    assert len(machine.o) == 0
    assert machine.d[0] == -11
    assert machine.h[1] == 4

def test_sample_print():
    file = open('programs/print.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 3
    assert len(machine.d) == 0
    assert len(machine.b) == 0
    assert len(machine.h) == 1
    assert len(machine.o) == 1
    assert machine.h[1] == 65
    assert machine.o == 'A'


def test_sample_procedures():
    file = open('programs/procedures.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 20
    assert len(machine.d) == 0
    assert len(machine.b) == 0
    assert len(machine.h) == 1
    assert len(machine.o) == 2
    assert machine.h[1] == 20
    assert machine.o == '35'

def test_sample_simple():
    file = open('programs/test.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 8
    assert len(machine.d) == 1
    assert len(machine.b) == 0
    assert len(machine.h) == 1
    assert len(machine.o) == 0
    assert machine.d[0] == -11
    assert machine.h[1] == 4


def test_sample_u06_a35():
    file = open('programs/u06_a35.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 8
    assert len(machine.d) == 1
    assert len(machine.b) == 0
    assert len(machine.h) == 2
    assert len(machine.o) == 0
    assert machine.d[0] == 125
    assert machine.h[1] == 5
    assert machine.h[2] == 10

def test_sample_u07_a38():
    file = open('programs/u07_a38.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 20
    assert len(machine.d) == 0
    assert len(machine.b) == 0
    assert len(machine.h) == 4
    assert len(machine.o) == 0
    assert machine.h[0] == 1
    assert machine.h[1] == 0
    assert machine.h[2] == 1
    assert machine.h[3] == 2

def test_sample_u08_a40():
    file = open('programs/u08_a40.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 29
    assert len(machine.d) == 0
    assert len(machine.b) == 0
    assert len(machine.h) == 2
    assert len(machine.o) == 0
    assert machine.h[0] == 2
    assert machine.h[1] == 1

def test_sample_u09_a36():
    file = open('programs/u09_a36.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 43
    assert len(machine.d) == 0
    assert len(machine.b) == 0
    assert len(machine.h) == 1
    assert len(machine.o) == 0
    assert machine.h[1] == 2

def test_sample_u09_a50():
    file = open('programs/u09_a50.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 24
    assert len(machine.d) == 0
    assert len(machine.b) == 0
    assert len(machine.h) == 0
    assert len(machine.o) == 0

def test_sample_u10_a52():
    file = open('programs/u10_a52.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 41
    assert len(machine.d) == 0
    assert len(machine.b) == 0
    assert len(machine.h) == 9
    assert len(machine.o) == 0
    assert machine.h[0] == 9
    assert machine.h[1] == 3
    assert machine.h[2] == 0
    assert machine.h[3] == 42
    assert machine.h[4] == 84
    assert machine.h[5] == 0
    assert machine.h[6] == 3
    assert machine.h[7] == 8
    assert machine.h[8] == 42

def test_sample_u11_a48():
    file = open('programs/u11_a48.zc', 'r')
    assert file

    zcode = file.read()
    file.close()
    assert len(zcode) > 0

    tokens = scanner.tokenize(zcode)
    parser = ZCodeParser(tokens)
    assert parser.valid()

    machine = Machine(ZFPDSInstructions(), tokens, {})
    assert len(machine.instructions) > 0

    machine.run()
    assert machine.m == 18
    assert len(machine.d) == 0
    assert len(machine.b) == 0
    assert len(machine.h) == 2
    assert len(machine.o) == 0
    assert machine.h[1] == 1
    assert machine.h[2] == 6
