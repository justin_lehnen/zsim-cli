from zsim.instruction_sets import InstructionSet, ZInstructions, ZFPInstructions, ZDSInstructions, ZFPDSInstructions


def test_inheritance():
    assert any([_class.__name__ == 'ZFPInstructions' for _class in ZFPDSInstructions.__bases__])
    assert any([_class.__name__ == 'ZDSInstructions' for _class in ZFPDSInstructions.__bases__])
    assert any([_class.__name__ == 'ZInstructions' for _class in ZFPInstructions.__bases__])
    assert any([_class.__name__ == 'ZInstructions' for _class in ZDSInstructions.__bases__])
    assert any([_class.__name__ == 'InstructionSet' for _class in ZInstructions.__bases__])

def test_indexer():
    zfpds = ZFPDSInstructions()
    assert zfpds['NOP'] == zfpds.NOP

def test_indexer_invalid():
    zfpds = ZFPDSInstructions()
    try:
        instruction = zfpds['NONEXISTANT']
        assert False
    except:
        assert True
