from zsim.instruction_sets import InstructionSet, ZInstructions, ZFPInstructions, ZDSInstructions, ZFPDSInstructions
from zsim.instructions import Instruction
from zsim import Machine


def test_instruction_LIT():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])

    zfpds('LIT', machine, 7)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == 7

def test_instruction_POP():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [7]

    assert machine.m == 1
    assert len(machine.d) == 1
    assert machine.d[0] == 7
    zfpds('POP', machine)
    assert machine.m == 2
    assert len(machine.d) == 0

def test_instruction_NOP():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])

    zfpds('NOP', machine)
    assert machine.m == 2

def test_instruction_LOD():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [], { 1: 20 })

    assert machine.m == 1
    assert len(machine.d) == 0
    zfpds('LOD', machine, 1)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == 20

def test_instruction_STO():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [], { 1: 20 })
    machine.d = [7]

    assert machine.m == 1
    assert len(machine.d) == 1
    assert machine.d[0] == 7
    assert len(machine.h) == 1
    assert machine.h[1] == 20
    zfpds('STO', machine, 1)
    assert machine.m == 2
    assert len(machine.d) == 0
    assert len(machine.h) == 1
    assert machine.h[1] == 7

def test_instruction_STO_new():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [], {})
    machine.d = [7]

    assert machine.m == 1
    assert len(machine.d) == 1
    assert machine.d[0] == 7
    assert len(machine.h) == 0
    zfpds('STO', machine, 1)
    assert machine.m == 2
    assert len(machine.d) == 0
    assert len(machine.h) == 1
    assert machine.h[1] == 7

def test_instruction_PRN():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [7]

    assert machine.m == 1
    assert len(machine.d) == 1
    assert machine.d[0] == 7
    assert len(machine.o) == 0
    zfpds('PRN', machine)
    assert machine.m == 2
    assert len(machine.d) == 0
    assert len(machine.o) == 2
    assert machine.o == '7\n'

def test_instruction_PRT():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [7]

    assert machine.m == 1
    assert len(machine.d) == 1
    assert machine.d[0] == 7
    assert len(machine.o) == 0
    zfpds('PRT', machine)
    assert machine.m == 2
    assert len(machine.d) == 0
    assert len(machine.o) == 1
    assert machine.o == '7'

def test_instruction_PRC():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [65]

    assert machine.m == 1
    assert len(machine.d) == 1
    assert machine.d[0] == 65
    assert len(machine.o) == 0
    zfpds('PRC', machine)
    assert machine.m == 2
    assert len(machine.d) == 0
    assert len(machine.o) == 1
    assert machine.o == 'A'

def test_instruction_ADD():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [6, 3]

    assert machine.m == 1
    assert len(machine.d) == 2
    assert machine.d[0] == 6
    assert machine.d[1] == 3
    zfpds('ADD', machine)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == 9

def test_instruction_SUB():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [6, 3]

    assert machine.m == 1
    assert len(machine.d) == 2
    assert machine.d[0] == 6
    assert machine.d[1] == 3
    zfpds('SUB', machine)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == 3

def test_instruction_MULT():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [6, 3]

    assert machine.m == 1
    assert len(machine.d) == 2
    assert machine.d[0] == 6
    assert machine.d[1] == 3
    zfpds('MULT', machine)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == 18

def test_instruction_DIV():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [6, 3]

    assert machine.m == 1
    assert len(machine.d) == 2
    assert machine.d[0] == 6
    assert machine.d[1] == 3
    zfpds('DIV', machine)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == 2

def test_instruction_JMP():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])

    assert machine.m == 1
    zfpds('JMP', machine, 10)
    assert machine.m == 10

def test_instruction_JMP_zero():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.instructions = [
        Instruction('NOP', zfpds.NOP), # 1 or -3
        Instruction('NOP', zfpds.NOP), # 2 or -2
        Instruction('NOP', zfpds.NOP), # 3 or -1
        # 0
    ]

    assert machine.m == 1
    zfpds('JMP', machine, 0)
    assert machine.m == len(machine.instructions) + 1

def test_instruction_JMP_negative():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.instructions = [
        Instruction('NOP', zfpds.NOP), # 1 or -3
        Instruction('NOP', zfpds.NOP), # 2 or -2
        Instruction('NOP', zfpds.NOP), # 3 or -1
        # 0
    ]

    assert machine.m == 1
    zfpds('JMP', machine, -2)
    assert machine.m == len(machine.instructions) - 1

def test_instruction_JMC():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [0]

    assert machine.m == 1
    zfpds('JMC', machine, 10)
    assert machine.m == 10
    assert len(machine.d) == 0

def test_instruction_JMC_false():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [1]

    assert machine.m == 1
    zfpds('JMC', machine, 10)
    assert machine.m == 2
    assert len(machine.d) == 0

def test_instruction_JMC_zero():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.instructions = [
        Instruction('NOP', zfpds.NOP), # 1 or -3
        Instruction('NOP', zfpds.NOP), # 2 or -2
        Instruction('NOP', zfpds.NOP), # 3 or -1
        # 4 or 0
    ]
    machine.d = [0]

    assert machine.m == 1
    zfpds('JMC', machine, 0)
    assert machine.m == len(machine.instructions) + 1
    assert len(machine.d) == 0

def test_instruction_JMC_negative():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.instructions = [
        Instruction('NOP', zfpds.NOP), # 1 or -3
        Instruction('NOP', zfpds.NOP), # 2 or -2
        Instruction('NOP', zfpds.NOP), # 3 or -1
        # 4 or 0
    ]
    machine.d = [0]

    assert machine.m == 1
    zfpds('JMC', machine, -2)
    assert machine.m == len(machine.instructions) - 1
    assert len(machine.d) == 0

def test_instruction_EQ():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [5, 5]

    assert machine.m == 1
    assert len(machine.d) == 2
    assert machine.d[0] == 5
    assert machine.d[1] == 5
    zfpds('EQ', machine)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == 1

def test_instruction_EQ_false():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [5, -5]

    assert machine.m == 1
    assert len(machine.d) == 2
    assert machine.d[0] == 5
    assert machine.d[1] == -5
    zfpds('EQ', machine)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == 0

def test_instruction_NE():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [5, -5]

    assert machine.m == 1
    assert len(machine.d) == 2
    assert machine.d[0] == 5
    assert machine.d[1] == -5
    zfpds('NE', machine)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == 1

def test_instruction_NE_false():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [5, 5]

    assert machine.m == 1
    assert len(machine.d) == 2
    assert machine.d[0] == 5
    assert machine.d[1] == 5
    zfpds('NE', machine)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == 0

def test_instruction_LT():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [3, 5]

    assert machine.m == 1
    assert len(machine.d) == 2
    assert machine.d[0] == 3
    assert machine.d[1] == 5
    zfpds('LT', machine)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == 1

def test_instruction_LT_false():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [5, 5]

    assert machine.m == 1
    assert len(machine.d) == 2
    assert machine.d[0] == 5
    assert machine.d[1] == 5
    zfpds('LT', machine)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == 0


def test_instruction_GT():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [5, -5]

    assert machine.m == 1
    assert len(machine.d) == 2
    assert machine.d[0] == 5
    assert machine.d[1] == -5
    zfpds('GT', machine)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == 1

def test_instruction_GT_false():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [5, 5]

    assert machine.m == 1
    assert len(machine.d) == 2
    assert machine.d[0] == 5
    assert machine.d[1] == 5
    zfpds('GT', machine)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == 0


def test_instruction_LE():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [3, 5]

    assert machine.m == 1
    assert len(machine.d) == 2
    assert machine.d[0] == 3
    assert machine.d[1] == 5
    zfpds('LE', machine)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == 1

def test_instruction_LE_equal():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [5, 5]

    assert machine.m == 1
    assert len(machine.d) == 2
    assert machine.d[0] == 5
    assert machine.d[1] == 5
    zfpds('LE', machine)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == 1

def test_instruction_LE_false():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [5, -5]

    assert machine.m == 1
    assert len(machine.d) == 2
    assert machine.d[0] == 5
    assert machine.d[1] == -5
    zfpds('LE', machine)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == 0

def test_instruction_GE():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [5, -5]

    assert machine.m == 1
    assert len(machine.d) == 2
    assert machine.d[0] == 5
    assert machine.d[1] == -5
    zfpds('GE', machine)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == 1

def test_instruction_GE_equal():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [5, 5]

    assert machine.m == 1
    assert len(machine.d) == 2
    assert machine.d[0] == 5
    assert machine.d[1] == 5
    zfpds('GE', machine)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == 1

def test_instruction_GE_false():
    zfpds = ZFPDSInstructions()
    machine = Machine(zfpds, [])
    machine.d = [3, 5]

    assert machine.m == 1
    assert len(machine.d) == 2
    assert machine.d[0] == 3
    assert machine.d[1] == 5
    zfpds('GE', machine)
    assert machine.m == 2
    assert len(machine.d) == 1
    assert machine.d[0] == 0
